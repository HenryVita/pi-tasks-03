#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define LEN 10
#define Z 10;

void rand_num(int* rannum)
{
	int i;
	int z=Z;
	srand(time(0));
	for (i=0;i<LEN;i++)
		rannum[i]=rand()%(z*2+1)-z;
}

void max_min_med(int* rannum,int* max,int* min,float* medium)
{
	long long int sum=0;
	int j;

	*min=*max=rannum[0];
	sum+=rannum[0];
	
	for(j=1;j<LEN;j++)
	{
		sum+=rannum[j];
		if (*min>rannum[j]) *min=rannum[j];	
		if (*max<rannum[j]) *max=rannum[j];	
	}
	*medium=(double)sum/LEN;
}

int main()
{
	int rannum[LEN];
	int max,min;
	int i;
	float medium;
	
	rand_num(rannum);
	max_min_med(rannum,&max,&min,&medium);
	for (i=0;i<LEN;i++)
		printf("%d ",rannum[i]);
	puts("");
	printf("min= %d, max= %d, medium= %.2f",min,max,medium);
	puts("");
	return 0;
}