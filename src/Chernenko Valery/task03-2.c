#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define LEN 100

int main()
{
	unsigned int rand_number[LEN];
	int i,curretMax,count,curretNum,num,repeat=1;

	srand(time(0));
	for (i=0;i<LEN;i++)
	{
		rand_number[i]=rand()%9+1;
	}
	
	count=curretMax=rand_number[0];
	num=curretNum=1;

	for (i=1;i<LEN;i++)
	{
		if (count==rand_number[i])
			num++;
		else
			num=1;
		if (num==curretNum)
			repeat=1;
		if (num>curretNum)
		{
			curretNum=num;
			curretMax=rand_number[i];
			repeat=0;
		}
		count=rand_number[i];
	}
	for (i=0;i<LEN;i++)
		printf("%d ",rand_number[i]);
	puts("");
	if (repeat)
	{
		printf("Sorry, in array there are several maximal sequences.\n");
		printf("it's first sequences of length %d >> ",curretNum);
			for (i=0;i<curretNum-1;i++)
				printf("%d, ",curretMax);
		printf("%d\n",curretMax);
	}
	else
	{
		printf("the desired sequence of length %d >> ",curretNum);
		for (i=0;i<curretNum-1;i++)
			printf("%d, ",curretMax);
		printf("%d\n",curretMax);
	}
	return 0;
}